import AppBar from "@/components/AppBar.vue";
import { createLocalVue, mount } from "@vue/test-utils";
import Vue from "vue";
import Vuetify from "vuetify";

let vuetify;
let localVue;

beforeAll(() => {
  Vue.use(Vuetify);
  vuetify = new Vuetify();
  localVue = createLocalVue();
});

describe("AppBar", () => {
  it("theme-btn should switch Vuetify theme and set localStorage", async () => {
    const wrapper = mount(AppBar, { localVue, vuetify });
    const currentThemeDark = wrapper.vm.$vuetify.theme.dark;

    wrapper.find('[data-testid="theme-btn"]').trigger("click");
    await wrapper.vm.$nextTick;

    expect(wrapper.vm.$vuetify.theme.dark).toBe(!currentThemeDark);
    expect(localStorage.getItem("prefersColorSchemeDark")).toBe(
      String(!currentThemeDark)
    );
  });
});

import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import ClientService from "@/services/Client";

Vue.use(Vuetify);

export default new Vuetify({
  theme: { dark: ClientService.prefersColorSchemeDark() },
});

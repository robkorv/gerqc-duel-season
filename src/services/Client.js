export default {
  prefersColorSchemeDark: () => {
    const local = localStorage.getItem("prefersColorSchemeDark");
    let result;

    if (local) {
      result = local === String(true);
    } else {
      result = window.matchMedia("(prefers-color-scheme: dark)").matches;
    }

    return result;
  },
};

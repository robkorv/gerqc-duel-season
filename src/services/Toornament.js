import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://api.toornament.com/viewer/v2",
  headers: {
    "X-Api-Key": process.env.VUE_APP_TOORNAMENT_API_KEY,
    Range: "items=0-49",
  },
});

export default {
  getTournament(tournamentId) {
    return apiClient.get(`/tournaments/${tournamentId}`);
  },
  getStages(tournamentId) {
    return apiClient.get(`/tournaments/${tournamentId}/stages`);
  },
  getStageRanking(tournamentId, stageId) {
    return apiClient.get(
      `/tournaments/${tournamentId}/stages/${stageId}/ranking-items`
    );
  },
};

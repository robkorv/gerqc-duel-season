module.exports = {
  chainWebpack: (config) => {
    config.optimization.minimizer("terser").tap((args) => {
      args[0].terserOptions.output = { comments: false };
      return args;
    });
    if (process.env.NODE_ENV === "production") {
      config.plugin("optimize-css").tap((args) => {
        args[0].cssnanoOptions.preset[1].discardComments = { removeAll: true };
        return args;
      });
    }
  },
  publicPath:
    process.env.NODE_ENV === "production" ? "/gerqc-duel-season/" : "/",
  transpileDependencies: ["vuetify"],
  productionSourceMap: false,
};
